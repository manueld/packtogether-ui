const axios = require('axios').default;

class ListService {

    constructor() {
        this.httpClient = axios.create({
            baseURL: '',
            timeout: 10000,
            headers: {}
        });
        this.httpClient.interceptors.response.use(response => {
            return response;
        }, error => {
            if (error.response && error.response.data && error.response.data) {
                return Promise.reject({ ...error.response.data });
            } else if (error.response && !isNaN(error.response.status)) {
                return Promise.reject({ errStatus: error.response.status });
            } else {
                return Promise.reject(error);
            }
        });
    }

    async loadLists() {
        return this.httpClient.get('/api/lists');
    }

    async addList(listTitle) {
        return this.httpClient.post('/api/lists', { listTitle });
    }

    async setItemCountPacked(listId, itemId, value) {
        return this.httpClient.put(`/api/lists/${listId}/items/${itemId}/counter?newValue=${value}`);
    }

    async saveNewItem(listId, groupId, fullTitle) {
        return this.httpClient.post(`/api/lists/${listId}/groups/${groupId}/items`, { fullTitle });
    }

    async saveNewGroup(listId, groupTitle) {
        return this.httpClient.post(`/api/lists/${listId}/groups`, { groupTitle });
    }
}

export default ListService;
