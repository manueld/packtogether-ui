const axios = require('axios').default;

class AuthService {

    constructor() {
        this.httpClient = axios.create({
            baseURL: '',
            timeout: 10000,
            headers: {}
        });
        this.httpClient.interceptors.response.use(response => {
            return response;
        }, error => {
            if (error.response && error.response.data && error.response.data) {
                return Promise.reject({ ...error.response.data });
            } else {
                return Promise.reject(error);
            }
        });
    }

    async signin(emailAddress, password) {
        console.log('Signing in to /auth with ', { emailAddress, password });
        const data = { emailAddress, password };
        const options = { timeout: 1500 };
        return this.httpClient.post('/api/auth', data, options);
    }

    async signup(emailAddress, firstName, lastName, password) {
        console.log('Signing up to /register with ', { emailAddress, firstName, lastName, password });
        const data = { emailAddress, firstName, lastName, password };
        const options = { timeout: 1500 };
        return this.httpClient.post('/api/register', data, options);
    }

    signout() {
        console.log('Signing up to /logout');
        return this.httpClient.post('/api/logout');
    }

}

export default AuthService;
