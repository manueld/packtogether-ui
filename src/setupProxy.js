/*
  This proxy is used for local development only!
  For production builds, there is a simple express webserver running (/runner.js)
*/

const proxy = require('http-proxy-middleware').createProxyMiddleware;

module.exports = app => {
    app.use(proxy('/api', { target: 'http://localhost:5599' }));
};
