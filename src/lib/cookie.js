export const getAuthTokenFromCookie = cookieName => {
    if (!document.cookie) { return null };
    const parsedCookies = document.cookie
        .split(';')
        .map(ce => String(ce).trim())
        .map(ce => ce.split('='))
        .reduce((container, item) => {
            container[item[0]] = item[1];
            return container;
        }, {});
    if (parsedCookies[cookieName]) {
        return parsedCookies[cookieName];
    }
    return null;
};
