import React from 'react';
import { Paper, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import ListItem from './ListItem';
import ListItemAdd from './ListItemAdd';

const useStyles = makeStyles((theme) => ({
    root: {
        textAlign: 'left'
    },
    paper: {
        padding: theme.spacing(1.5)
    },
    groupHeader: {
        marginTop: 0,
        marginBottom: 6
    }
}));

function ListGroup(props) {
    const classes = useStyles();

    return (
        <Grid item xs={12} md={4} className={classes.root}>
            <Paper className={classes.paper}>
                <h3 className={classes.groupHeader}>{props.data.title}</h3>
                <div>
                    {props.data.items.map((item) => (
                        <ListItem data={item} listId={props.listId} key={item.itemId} />
                    ))}
                    <ListItemAdd listId={props.listId} groupId={props.data.groupId} />
                </div>
            </Paper>
        </Grid>
    );
}

export default ListGroup;
