import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import {
    Grid,
    Backdrop,
    CircularProgress,
    Toolbar,
    Typography,
    Button,
    Menu,
    MenuItem,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogContentText,
    DialogActions,
    TextField
} from '@material-ui/core';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import ListGroup from './ListGroup';
import { loadListsSend, saveNewGroup } from '../redux/actions/listActions';

const useStyles = makeStyles((theme) => ({
    toolbar: {
        marginBottom: theme.spacing(1),
        paddingLeft: 0,
        paddingRight: 0
    },
    title: {
        flexGrow: 1,
        fontSize: '1.5rem'
    },
    paper: {
        minHeight: 2,
        padding: 20,
        '&:hover': {
            backgroundColor: '#eee'
        }
    },
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
    },
}));

function ListsDetail(props) {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [titleNewGroup, setTitleNewGroup] = React.useState('');
    const [openNewGroupDialog, setOpenNewGroupDialog] = React.useState(false);
    const dispatch = useDispatch();
    const classes = useStyles();
    const loaded = useSelector(state => state.lists.loaded);
    const isLoading = useSelector(state => state.lists.isLoading);
    const list = useSelector(state => state.lists.data.find(l => l.listId === props.listId));

    useEffect(() => {
        if (!loaded && !isLoading) {
            dispatch(loadListsSend());
        }
    });

    const handleMenuButtonClick = (e) => {
        setAnchorEl(e.currentTarget);
    };

    const handleMenuClose = () => {
        setAnchorEl(null);
    }

    const handleMenuAddGroupClick = () => {
        setAnchorEl(null);
        setOpenNewGroupDialog(true);
    };

    const handleTitleNewGroupChange = (e) => {
        setTitleNewGroup(e.target.value);
    };

    const handleSubmitNewGroupDialog = () => {
        dispatch(saveNewGroup(list.listId, titleNewGroup));
        setOpenNewGroupDialog(false);
        setTitleNewGroup('');
    }

    const handleCloseNewGroupDialog = () => {
        setOpenNewGroupDialog(false);
        setTitleNewGroup('');
    }

    const render = () => {
        if (isLoading || !loaded) {
            return (
                <Backdrop className={classes.backdrop} open={true}>
                    <CircularProgress color="inherit" />
                </Backdrop>
            );
        }
        return list ? (
            <div className={classes.root}>
                <Toolbar className={classes.toolbar}>
                    <Link to={'/'} color="primary">
                        <Button edge="start" className={classes.menuButton} color="primary" aria-label="back">
                            <ArrowBackIosIcon />
                        </Button>
                    </Link>
                    <Typography variant="h3" className={classes.title}>{list.title}</Typography>
                    <Button color="inherit" aria-controls="list-menu" aria-haspopup="true" onClick={handleMenuButtonClick}>
                        <MoreVertIcon />
                    </Button>
                    <Menu
                        id="list-menu"
                        anchorEl={anchorEl}
                        keepMounted
                        elevation={0}
                        transformOrigin={{
                            vertical: 'top',
                            horizontal: 'center',
                        }}
                        open={Boolean(anchorEl)}
                        onClose={handleMenuClose}
                    >
                        <MenuItem onClick={handleMenuAddGroupClick}>Add group</MenuItem>
                        {/*
                        <MenuItem onClick={handleMenuClose}>Duplicate this list</MenuItem>
                        <MenuItem onClick={handleMenuClose}>Share...</MenuItem>
                        */}
                    </Menu>
                    <Dialog open={openNewGroupDialog} onClose={handleCloseNewGroupDialog} aria-labelledby="form-dialog-title">
                        <DialogTitle id="form-dialog-title">New group</DialogTitle>
                        <DialogContent>
                            <DialogContentText>
                                To create a new group, define a describing title for it.
                            </DialogContentText>
                            <TextField
                                value={titleNewGroup}
                                autoFocus
                                margin="dense"
                                id="groupTitle"
                                label="New group title"
                                type="text"
                                fullWidth
                                onChange={handleTitleNewGroupChange}
                            />
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={handleCloseNewGroupDialog} color="primary">Cancel</Button>
                            <Button onClick={handleSubmitNewGroupDialog} color="primary">Add</Button>
                        </DialogActions>
                    </Dialog>
                </Toolbar>
                <Grid container spacing={3} className="lists-detail-content">
                    {list.groups.map((group) => (
                        <ListGroup data={group} listId={props.listId} key={group.groupId} />
                    ))}
                </Grid>
            </div>
        ) : null;
    } 

    return render();
}

export default ListsDetail;
