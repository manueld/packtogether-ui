import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Checkbox } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { setItemCountPacked } from "../redux/actions/listActions"; 

const useStyles = makeStyles((theme) => ({
  checkbox: {
      padding: 0
  }
}));

function ListItem(props) {
  const dispatch = useDispatch();
  const classes = useStyles();
  const listItem = props.data;

  const [checkboxMouseDown, setCheckboxMouseDown] = useState({
    timer: null,
    rangeSelectorActive: false,
    startX: -1,
    startY: -1,
    newValue: listItem.countPacked || -1
  });
  const [labelHovered, setLabelHovered] = useState(false);

  const handleItemMouseMove = (e) => {
    if (e.target.parentNode.classList.contains('list-item-label') || e.target.classList.contains('list-item-label')) {
      setLabelHovered(true);
    }
  }

  const handleItemMouseOut = () => {
    setLabelHovered(false);
  }

  const handleCheckboxClick = (e) => {
    // If already "complete", an untick results in setting countPacked to 0
    // Otherwise, we set it to complete (countPacked = count)
    window.removeEventListener("mousemove", handleMouseMove);
    window.removeEventListener("mouseup", handleCheckboxMouseUp);
    window.removeEventListener("touchmove", handleMouseMove);
    window.removeEventListener("touchend", handleCheckboxMouseUp);
    window.removeEventListener("touchcancel", handleCheckboxMouseUp);
    if (checkboxMouseDown.timer) {
      window.clearTimeout(checkboxMouseDown.timer);
    }
    if (checkboxMouseDown.rangeSelectorActive) {
      e.preventDefault();
      return;
    }
    const ticked = e.target.checked;
    const newCountPacked = ticked ? listItem.count : 0;
    dispatch(setItemCountPacked(props.listId, listItem.itemId, newCountPacked));
    setCheckboxMouseDown(prevState => ({
      ...prevState,
      timer: null,
      rangeSelectorActive: false
    }));
    dispatch({ type: 'LIST_ITEM_RANGESELECTOR', payload: { active: false } });
  }

  const handleTitleClick = (e) => {
    if (listItem.countPacked < listItem.count) {
      const newCountPacked = listItem.countPacked + 1;
      dispatch(setItemCountPacked(props.listId, listItem.itemId, newCountPacked));
    }
  }

  const handleCheckboxMouseDown = (e) => {
    e.persist();
    if (e.target) {
      if (!checkboxMouseDown.timer) {
        setCheckboxMouseDown({
          timer: window.setTimeout(useCheckboxRangeSelector, 400),
          rangeSelectorActive: false,
          startX: e.clientX,
          startY: e.clientY,
          newValue: listItem.countPacked
        });
        dispatch({ type: 'LIST_ITEM_RANGESELECTOR', payload: { active: false } });
      }
    }
  }

  const handleMouseMove = (cbX, cbY, e) => {
    const getMovementInfo = () => {
      if (cbX === e.clientX && cbY === e.clientY) {
        return { direction: "", distance: 0 };
      }
      const direction =
        cbX < e.clientX || cbY > e.clientY
          ? "UP"
          : "DOWN";
      const distance = Math.sqrt(
        Math.pow(cbX - e.clientX, 2) +
          Math.pow(cbY - e.clientY, 2)
      );
      return { direction, distance };
    };
    const calcNewValueFromDistance = (
      moveInfo,
      distanceValue,
      currentValue,
      maxValue
    ) => {
      if (
        !moveInfo ||
        !moveInfo.direction ||
        !["UP", "DOWN"].includes(moveInfo.direction)
      ) {
        return currentValue;
      }
      switch (moveInfo.direction) {
        case "UP":
          return Math.min(maxValue, currentValue + distanceValue);
        case "DOWN":
          return Math.max(0, currentValue - distanceValue);
        default:
          return currentValue;
      }
    };

    //console.log(`[handleMouseMove] current: ${e.clientX}:${e.clientY} // ${cbX}:${cbY}`);
    if (cbX < 0 || cbY < 0) {
      return;
    }
    const moveInfo = getMovementInfo();
    const distanceValue = moveInfo ? Math.floor(moveInfo.distance / 30) : 0; // 30 is the config for 1 "step"
    const newValue = calcNewValueFromDistance(
      moveInfo,
      distanceValue,
      listItem.countPacked,
      listItem.count
    );
    //console.log(`[handleMouseMove]`, { distanceValue, newValue });
    if (newValue !== checkboxMouseDown.newValue) {
      setCheckboxMouseDown(prevState => ({
        ...prevState,
        newValue
      }));
    }
  };

  const handleCheckboxMouseUp = (e) => {
    window.removeEventListener("mousemove", handleMouseMove);
    window.removeEventListener("mouseup", handleCheckboxMouseUp);
    window.removeEventListener("touchmove", handleMouseMove);
    window.removeEventListener("touchend", handleCheckboxMouseUp);
    window.removeEventListener("touchcancel", handleCheckboxMouseUp);
    setCheckboxMouseDown(prevState => {
      if (prevState.rangeSelectorActive) {
        dispatch(setItemCountPacked(props.listId, listItem.itemId, prevState.newValue));
      }
      return {
        timer: null,
        rangeSelectorActive: false,
        startX: -1,
        startY: -1,
        newValue: null
      };
    });
    dispatch({ type: 'LIST_ITEM_RANGESELECTOR', payload: { active: false } });
  };

  const useCheckboxRangeSelector = () => {
    window.removeEventListener("mousemove", handleMouseMove);
    window.removeEventListener("mouseup", handleCheckboxMouseUp);
    window.removeEventListener("touchmove", handleMouseMove);
    window.removeEventListener("touchend", handleCheckboxMouseUp);
    window.removeEventListener("touchcancel", handleMouseMove);
    
    if (checkboxMouseDown.rangeSelectorActive) {
      console.warn('[useCheckboxRangeSelector] disabled event listeners again because of active range selector');
      return;
    }

    
    setCheckboxMouseDown(prevState => {
      window.clearTimeout(prevState.timer);
      window.addEventListener("mousemove", handleMouseMove.bind(null, prevState.startX, prevState.startY));
      window.addEventListener("mouseup", handleCheckboxMouseUp);
      window.addEventListener("touchmove", handleMouseMove.bind(null, prevState.startX, prevState.startY));
      window.addEventListener("touchend", handleCheckboxMouseUp);
      window.addEventListener("touchcancel", handleCheckboxMouseUp);
      return {
        ...prevState,
        rangeSelectorActive: true
      };
    });
    dispatch({ type: 'LIST_ITEM_RANGESELECTOR', payload: { active: true } });
  };

  const getCurrentCountPacked = () => {
    return checkboxMouseDown.rangeSelectorActive ? checkboxMouseDown.newValue : listItem.countPacked;
  };

  const getItemClassName = () => {
    const cls = ['list-item'];
    if (getCurrentCountPacked() === listItem.count) {
      cls.push('complete');
    }
    if (labelHovered && listItem.count > 1) {
      cls.push('label-hovered');
    }
    if (checkboxMouseDown.rangeSelectorActive) {
      cls.push('range-selector-active');
    }
    return cls.join(' '); 
  };

  const renderCounter = () => {
    if (listItem.count === 1) {
      return null;
    }
    return `${getCurrentCountPacked()}/${listItem.count}`;
  };

  return (
    <div
      className={getItemClassName()}
      onMouseMove={(e) => handleItemMouseMove(e)}
      onMouseOut={(e) => handleItemMouseOut(e)}
    >
      <span className="list-item-check">
          <span className="list-item-check-inc">+1</span> 
          <Checkbox
            value={listItem.countPacked}
            checked={listItem.count === listItem.countPacked}
            indeterminate={listItem.countPacked > 0 && listItem.count > listItem.countPacked}
            color="primary"
            disableRipple={true}
            className={classes.checkbox}
            onClick={(e) => handleCheckboxClick(e)}
            onMouseDown={(e) => handleCheckboxMouseDown(e)}
            onTouchStart={(e) => handleCheckboxMouseDown(e)}
            onTouchEnd={(e) => handleCheckboxMouseUp(e)}
          />
      </span>
      <span className="list-item-label">
        <span
          className={`list-item-label-counter${
            checkboxMouseDown.rangeSelectorActive ? " active" : ""
          }`}
        >
          {" "}
          {renderCounter()}
        </span>
        {" "}
        <span onClick={(e) => handleTitleClick(e)}>{listItem.title}</span>
      </span>
    </div>
  );

}

export default ListItem;
