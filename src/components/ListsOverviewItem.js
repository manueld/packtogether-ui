import React from 'react';
import { Link } from 'react-router-dom';
import { Grid, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch } from 'react-redux';

const useStyles = makeStyles((theme) => ({
    paper: {
        minHeight: 2,
        padding: 20,
        '&:hover': {
            backgroundColor: '#eee'
        }
    },
    link: {
        textDecoration: 'none'
    }
}));

function ListsOverviewItem(props) {
    const classes = useStyles();
    const dispatch = useDispatch();

    return (
        <Grid item xs={12} md={6} lg={6} className="lists-overview-item" key={props.data.listId} onClick={() => dispatch({ type:'SET_LIST', payload: { listId: props.data.listId } })}>
            <Link to={`/lists/${props.data.listId}`} className={classes.link}>
                <Paper className={classes.paper}>
                    {props.data.title}
                </Paper>
            </Link>
        </Grid>
    );
}

export default ListsOverviewItem;
