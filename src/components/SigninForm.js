import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Button, TextField } from '@material-ui/core';
import { signinSend } from '../redux/actions/authActions';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginBottom: theme.spacing(1),
    display: 'block',
    width: '25ch',
  },
  buttonContainer: {
    '& > *': {
      margin: theme.spacing(0.5),
    },
  },
  registerLink: {

  }
}));

function SigninForm(props) {
  const [formData, setFormData] = useState({
    emailAddress: '',
    password: ''
  });
  const classes = useStyles();
  const dispatch = useDispatch();
  const isLoading = useSelector(state => state.auth && state.auth.isLoading);
  const errMessage = useSelector(state => state.auth && (state.auth.errKey || state.auth.errMessage || null));

  const setValue = fieldName => e => {
    setFormData({
      ...formData,
      [fieldName]: e.target.value
    });
  };

  return (
    <div className={classes.paper}>
      <h3>Log in</h3>
      <form action="" method="POST" onSubmit={(e) => e.preventDefault()}>
        <TextField id="register-email" type="email" size="small" label="EMail Address" variant="outlined" onChange={setValue('emailAddress')} className={classes.textField} margin="none" />
        <TextField id="register-password" type="password" size="small" label="Password" variant="outlined" onChange={setValue('password')} className={classes.textField} margin="none" />
        {errMessage ? <p>{errMessage}</p> : null}
        <div className={classes.buttonContainer}>
          <Button variant="outlined" color="primary" onClick={() => dispatch(signinSend(formData.emailAddress, formData.password))} disabled={isLoading}>{isLoading ? 'Signin in...' : 'Login'}</Button>
        </div>
      </form>
      <Typography color="inherit">
        <Button variant="text" onClick={() => props.history.push('/register')} color="inherit">Not registered yet? Sign up now!</Button>
      </Typography>
    </div>
  );
}

export default withRouter(SigninForm);
