import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { TextField } from "@material-ui/core";
import { saveNewItem } from "../redux/actions/listActions";

function ListItemAdd(props) {
  const dispatch = useDispatch();
  const [newTitle, setNewTitle] = useState('');

  const handleTextfieldChange = (e) => {
    setNewTitle(e.target.value);
  };

  const handleTextfieldKeyDown = (e) => {
    if (e.keyCode === 13) {
      dispatch(saveNewItem(props.listId, props.groupId, newTitle));
      setNewTitle('');
    }
    return true;
  };

  return (
    <div>
      <span className="list-item-add-title">
        <TextField
          id={`item-${props.groupId}-add`}
          size="small"
          label="New entry"
          onChange={(e) => handleTextfieldChange(e)}
          onKeyDown={(e) => handleTextfieldKeyDown(e)}
          value={newTitle}
        />
      </span>
    </div>
  );

}

export default ListItemAdd;
