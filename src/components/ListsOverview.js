import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import {
    Grid,
    Paper,
    Button,
    Container,
    Typography,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    TextField
} from '@material-ui/core';
import ListsOverviewItem from './ListsOverviewItem';
import { loadListsSend, addList } from '../redux/actions/listActions';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    introContainer: {
        display: 'flex',
        justifyContent: 'center'
    },
    introPaper: {
        paddingLeft: theme.spacing(10),
        paddingRight: theme.spacing(10),
        paddingTop: theme.spacing(5),
        paddingBottom: theme.spacing(5)
    },
    introButton: {
        marginTop: theme.spacing(4)
    }
}));

function ListsOverview(props) {
    const classes = useStyles();
    const dispatch = useDispatch();
    const isLoading = useSelector(state => state.lists.isLoading);
    const loaded = useSelector(state => state.lists.loaded);
    const [openNewListDialog, setOpenNewListDialog] = useState(false);
    const [titleNewList, setTitleNewList] = useState('');
    const [isFirstList, setisFirstList] = useState(false);

    useEffect(() => {
        if (!loaded && !isLoading) {
            dispatch(loadListsSend());
        }
    });

    const handleIntroButtonClick = () => {
        setOpenNewListDialog(true);
        setisFirstList(true);
    };

    const handleCloseNewListDialog = () => {
        setOpenNewListDialog(false);
        setTitleNewList('');
    };

    const handleTitleNewListChange = (e) => {
        setTitleNewList(e.target.value);
    };

    const handleAddListButtonClick = () => {
        dispatch(addList(titleNewList, props.history, isFirstList));
        setOpenNewListDialog(false);
        setTitleNewList('');
    }

    const shouldShowIntro = () => loaded && (!props.lists.data || props.lists.data.length === 0);

    const renderIntro = () => {
        return (
            <Container className={classes.introContainer}>
                <Paper className={classes.introPaper}>
                    <Typography variant="h5">It looks you have no lists created yet.</Typography>
                    <Button variant="outlined" className={classes.introButton} onClick={handleIntroButtonClick}>Create your first list</Button>
                </Paper>
            </Container>
        );
    };

    const renderList = () => props.lists.data.map(list => <ListsOverviewItem data={list} key={list.listId} />);

    return (
        <div className={classes.root}>
            <Grid container spacing={3} className="lists-overview-header">
                <Grid item xs>
                    <h3>Your lists</h3>
                </Grid>
            </Grid>
            <Grid container spacing={3} className="lists-overview-content">
                {shouldShowIntro() ? renderIntro() : renderList()}
            </Grid>
            <Dialog open={openNewListDialog} onClose={handleCloseNewListDialog} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">New list</DialogTitle>
                <DialogContent>
                    <TextField
                        value={titleNewList}
                        autoFocus
                        margin="dense"
                        id="listTitle"
                        label="New list title"
                        type="text"
                        fullWidth
                        onChange={handleTitleNewListChange}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseNewListDialog} color="primary">Cancel</Button>
                    <Button onClick={handleAddListButtonClick} color="primary">Add</Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default withRouter(ListsOverview);
