import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Container, Grid, CssBaseline } from '@material-ui/core';
import AppHeader from './components/general/AppHeader';
import SigninForm from './components/SigninForm';
import ListsOverview from './components/ListsOverview';
import ListsDetail from './components/ListsDetail';
import SignupForm from './components/SignupForm';
import './App.css';

function App() {
  const state = useSelector(state => state);
  const isLoggedIn = useSelector(state => 'token' in state.auth && !!state.auth.token && !state.auth.isLoading);
  const lists = useSelector(state => state.lists);
  const isListItemRangeSelectorActive = useSelector(state => state.listItemRangeSelectorActive || false);
  const [showDebug, setShowDebug] = useState(false);

  const toggleDebugging = () => {
    setShowDebug(!showDebug);
  };

  const renderDebugging = () => {
    if (!showDebug) {
      return null;
    }
    return (
      <React.Fragment>
        <code>{JSON.stringify(state)}</code><br />
        <code>
          isLoggedIn: {isLoggedIn ? 'true' : 'false'}<br />
          lists: {lists ? 'true' : 'false'}<br />
          isListItemRangeSelectorActive: {isListItemRangeSelectorActive ? 'true' : 'false'}
        </code>
      </React.Fragment>
    );
  };

  return (
    <div className={`app${isListItemRangeSelectorActive ? ' list-item-rangeselector-active' : ''}`}>
      {renderDebugging()}
      <Router>
        <CssBaseline />
        <Grid container>
          <Grid item xs={12}>
            <AppHeader isLoggedIn={isLoggedIn} titleOnClick={toggleDebugging} />
          </Grid>
          <Container component="main" maxWidth="md">
            {isLoggedIn && lists ? (
              <Switch>
                <Route path="/lists/:listId" render={({ match }) => (
                  <ListsDetail listId={match.params.listId} />
                )} />
                <Route path="/" render={() => (
                  <ListsOverview lists={lists} />
                )} />
              </Switch>
            ) : (
                <Switch>
                  <Route path="/register" exact={true} render={() => (
                    <SignupForm />
                  )} />
                  <Route component={SigninForm} />
                </Switch>
              )}
          </Container>
        </Grid>
      </Router>
    </div>
  );
}

export default App;
