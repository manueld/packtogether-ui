import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import appStateReducer from './reducer';

const store = createStore(appStateReducer, applyMiddleware(thunk));

export default store;
