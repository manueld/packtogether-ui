const ListService = require('../../services/ListService').default;

const listService = new ListService();

export const loadListsSend = () => {
    return dispatch => {
        // First, dispatch loading
        dispatch({ type: 'SEND_LISTS_GET' });
        listService.loadLists()
            .then(result => {
                console.log('ListService Successful Response (loadLists)', result);
                // Dispatch success result
                const listData = result.data.data || [];
                dispatch({
                    type: 'SET_LISTS_SUCCESS',
                    payload: { data: listData }
                });
            })
            .catch(err => {
                // Dispatch failed result
                console.log('ListService Failed Response', err);
                dispatch({
                    type: 'SET_LISTS_FAILURE',
                    payload: {
                        errMessage: err.message,
                        errKey: err.errKey,
                        errStatus: err.errStatus
                    }
                });
            });
    };
};

export const addList = (listTitle, history, redirectToNewList = false) => {
    return dispatch => {
        dispatch({ type: 'SEND_LIST_ADD' });
        listService.addList(listTitle)
            .then(result => {
                console.log('ListService Successful Response (addList)', result);
                dispatch({
                    type: 'SET_LISTS_SUCCESS',
                    payload: { data: result.data.data }
                });
                if (redirectToNewList && result.data.newListId) {
                    history.push(`/lists/${result.data.newListId}`);
                }
            })
            .catch(err => {
                console.log('ListService Failed Response', err);
                dispatch({
                    type: 'SET_LISTS_FAILURE',
                    payload: {
                        errMessage: err.message,
                        errKey: err.errKey,
                        errStatus: err.errStatus
                    }
                });
            });
    };
};

export const setItemCountPacked = (listId, itemId, countPacked) => {
    return dispatch => {
        // First, dispatch loading
        dispatch({
            type: 'UPDATE_LIST_ITEM',
            payload: {
                listId,
                itemId,
                newCountPacked: countPacked
            }
        });
        listService.setItemCountPacked(listId, itemId, countPacked)
            .then(() => {
                dispatch({
                    type: 'UPDATE_LIST_ITEM_SUCCESS',
                    payload: {
                        listId,
                        itemId,
                        newCountPacked: countPacked
                    }
                });
            })
            .catch(err => {
                // Dispatch failed result
                dispatch({
                    type: 'UPDATE_LIST_ITEM_FAILURE',
                    payload: {
                        errMessage: err.message,
                        errKey: err.errKey,
                        errStatus: err.errStatus
                    }
                });
            });
    };
};

export const saveNewItem = (listId, groupId, title) => {
    return dispatch => {
        dispatch({
            type: 'ADD_LIST_ITEM',
            payload: {
                listId,
                groupId,
                fullTitle: title
            }
        });
        listService.saveNewItem(listId, groupId, title)
            .then(result => {
                dispatch({
                    type: 'ADD_LIST_ITEM_SUCCESS',
                    payload: {
                        listId,
                        groupId,
                        newItem: result.data.data
                    }
                });
            })
            .catch(err => {
                dispatch({
                    type: 'ADD_LIST_ITEM_FAILURE',
                    payload: {
                        errMessage: err.message,
                        errKey: err.errKey,
                        errStatus: err.errStatus
                    }
                });
            });
    };
};

export const saveNewGroup = (listId, groupTitle) => {
    return dispatch => {
        dispatch({
            type: 'ADD_LIST_GROUP',
            payload: {
                listId,
                groupTitle
            }
        });
        listService.saveNewGroup(listId, groupTitle)
            .then(result => {
                dispatch({
                    type: 'ADD_LIST_GROUP_SUCCESS',
                    payload: {
                        listId,
                        newGroup: result.data.data
                    }
                });
            })
            .catch(err => {
                dispatch({
                    type: 'ADD_LIST_GROUP_FAILURE',
                    payload: {
                        errMessage: err.message,
                        errKey: err.errKey,
                        errStatus: err.errStatus
                    }
                });
            });
    };
};
