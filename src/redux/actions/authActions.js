const AuthService = require('../../services/AuthService').default;
const { getAuthTokenFromCookie } = require('../../lib/cookie');

const authService = new AuthService();

export const signinSend = (emailAddress, password) => {
    return dispatch => {
        // First, dispatch loading
        dispatch({ type: 'SEND_AUTH' });
        authService.signin(emailAddress, password)
            .then(result => {
                console.log('AuthService Successful Response', result);
                // Dispatch success result
                dispatch({
                    type: 'SET_AUTH',
                    payload: { token: getAuthTokenFromCookie('authToken') }
                });
            })
            .catch(err => {
                // Dispatch failed result
                console.log('AuthService Failed Response', err);
                dispatch({
                    type: 'SET_AUTH',
                    payload: {
                        errMessage: err.message,
                        errKey: err.errKey
                    }
                });
            });
    };
};

export const signupSend = ({ emailAddress, firstName, lastName, password }, history) => {
    return dispatch => {
        // First, dispatch loading
        dispatch({ type: 'SEND_REGISTER' });
        authService.signup(emailAddress, firstName, lastName, password)
            .then(result => {
                console.log('AuthService > signupSend: Successful Response', result);
                // Dispatch success result
                dispatch({
                    type: 'SET_AUTH',
                    payload: { token: getAuthTokenFromCookie('authToken') }
                });
                history.push(`/`);
            })
            .catch(err => {
                // Dispatch failed result
                console.log('AuthService > signupSend: Failed Response', err);
                dispatch({
                    type: 'SET_AUTH',
                    payload: {
                        errMessage: err.message,
                        errKey: err.errKey
                    }
                });
            });
    };
};

export const signoutSend = (redirectToHome, history) => {
    return dispatch => {
        dispatch({ type: 'UNSET_AUTH' });
        authService.signout()
            .then(() => {
                console.log('AuthService > signoutSend: Successful Response');
                // Dispatch success result
                if (redirectToHome && history) {
                    history.push('/');
                }
            })
            .catch(err => {
                // Dispatch failed result
                console.log('AuthService > signoutSend: Failed Response', err);
                if (redirectToHome && history) {
                    history.push('/');
                }
            });
    };
};
