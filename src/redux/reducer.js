const { getAuthTokenFromCookie } = require('../lib/cookie');

const initialState = Object.freeze({
    auth: {
        token: getAuthTokenFromCookie('authToken'),
        name: null,
        isLoading: false
    },
    lists: {
        loaded: false,
        isLoading: false,
        data: [],
        currentList: null
    }
});

const getListItem = (lists, listId, itemId) => {
    return lists.reduce((acc, list) => {
        if (list.listId === listId) {
            list.groups.forEach(group => {
                group.items.forEach(item => {
                    if (item.itemId === itemId) {
                        acc = item;
                        return acc;
                    }
                });
            });
        }
        return acc;
    }, null);
};

const getListGroup = (lists, listId, groupId) => {
    return lists.reduce((acc, list) => {
        if (list.listId === listId) {
            list.groups.forEach(group => {
                if (group.groupId === groupId) {
                    acc = group;
                    return acc;
                }
            });
        }
        return acc;
    }, null);
};

const appStateReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SEND_AUTH': {
            return {
                ...state,
                auth: {
                    ...state.auth,
                    isLoading: true
                }
            };
        }
        case 'SET_AUTH': {
            return {
                ...state,
                auth: {
                    ...action.payload,
                    isLoading: false
                },
                lists: {
                    loaded: false,
                    isLoading: false,
                    data: [],
                    currentList: null
                }
            };
        }
        case 'UNSET_AUTH': {
            return { ...initialState };
        }
        case 'SEND_LISTS_GET': {
            return {
                ...state,
                lists: {
                    ...state.lists,
                    loaded: false,
                    isLoading: true
                }
             };
        }
        case 'SEND_REGISTER': {
            return {
                ...state,
                auth: {
                    ...state.auth,
                    isLoading: true
                }
            };
        }

        case 'SET_LISTS_SUCCESS': {
            return {
                ...state,
                lists: {
                    loaded: true,
                    isLoading: false,
                    data: action.payload.data,
                    currentList: null,
                    errMessage: null,
                    errKey: null
                }
            }
        }
        case 'SET_LISTS_FAILURE': {
            const auth = { ...state.auth };
            if (action.payload.errStatus === 401) {
                // Auth token has been invalidated
                auth.token = null;
                auth.name = null;
            }
            return {
                ...state,
                auth,
                lists: {
                    ...action.payload,
                    loaded: true,
                    isLoading: false,
                    data: [],
                    currentList: null
                }
            }
        }
        case 'SET_LIST': {
            return {
                ...state,
                lists: {
                    ...state.lists,
                    currentList: action.payload.listId
                }
            }
        }
        case 'UPDATE_LIST_ITEM': {
            return state; // No update for now...
        }
        case 'UPDATE_LIST_ITEM_SUCCESS': {
            const item = getListItem(state.lists.data, action.payload.listId, action.payload.itemId);

            if (!isNaN(action.payload.newCountPacked)) {
                item.countPacked = action.payload.newCountPacked;
            }
            return {
                ...state,
                lists: {
                    ...state.lists,
                    currentList: action.payload.listId
                }
            }
        }
        case 'UPDATE_LIST_ITEM_FAILURE': {
            alert(action.payload.errMessage || action.payload.errKey || action.payload.errStatus);
            return state; // No update for now...
        }
        case 'ADD_LIST_ITEM': {
            const group = getListGroup(state.lists.data, action.payload.listId, action.payload.groupId);
            group.isLoading = true;
            return state;
        }
        case 'ADD_LIST_ITEM_SUCCESS': {
            const group = getListGroup(state.lists.data, action.payload.listId, action.payload.groupId);
            group.isLoading = false;
            group.items.push(action.payload.newItem);
            return {
                ...state,
                lists: {
                    ...state.lists
                }
            };
        }
        case 'ADD_LIST_ITEM_FAILURE': {
            alert(action.payload.errMessage || action.payload.errKey || action.payload.errStatus);
            const group = getListGroup(state.lists.data, action.payload.listId, action.payload.groupId);
            group.isLoading = false;
            return state; // No update for now...
        }
        case 'ADD_LIST_GROUP': {
            return {
                ...state,
                lists: {
                    ...state.lists,
                    loaded: false,
                    isLoading: true
                }
             };
        }
        case 'ADD_LIST_GROUP_SUCCESS': {
            const list = state.lists.data.find(list => list.listId === action.payload.listId);
            list.groups.push(action.payload.newGroup);
            return {
                ...state,
                lists: {
                    ...state.lists,
                    loaded: true,
                    isLoading: false
                }
             };
        }
        case 'LIST_ITEM_RANGESELECTOR': {
            return {
                ...state,
                listItemRangeSelectorActive: action.payload.active
            };
        }
        default: {
            return state;
        }
    }
};

export default appStateReducer;
