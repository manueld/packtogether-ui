FROM node:12.18-buster-slim

# Create app directory
RUN mkdir -p /home/node
WORKDIR /home/node

# Install app dependencies
COPY package.json package-lock.json server.js /home/node/
RUN npm install

# Bundle app source
COPY src /home/node/src/
COPY public /home/node/public
USER node

# Create React production app
RUN npm run-script build

EXPOSE 5588

CMD [ "npm", "start" ]