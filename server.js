const express = require('express');
const { createProxyMiddleware } = require('http-proxy-middleware');
 
const app = express();

const apiProxyHost = process.env['API_HOST'] || 'http://localhost:5599';
app.use('/api', createProxyMiddleware({ target: apiProxyHost, changeOrigin: true }));

app.use(express.static('build'));

app.listen(5588, () => {
    console.log(`UI Server running (port: 5588; API proxy: ${apiProxyHost})`);
});
